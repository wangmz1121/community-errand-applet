/* 导入ip端口文件 */
import { ipConFig } from '../utils/ipconfig'

// 首页模块
const indexApi = {
  // demo
  demo: () => {
    return ipConFig + '具体业务路径'
  }
}

module.exports = {
  indexApi
}