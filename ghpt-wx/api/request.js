// let commonUrl = 'https://46942r7177.goho.co/'
let commonUrl = 'https://wx.ymgh.cloud/'

// post请求封装
function postRequest(url, data) {
  var promise = new Promise((resolve, reject) => {
      var postData = data;
      wx.request({
          url: commonUrl + url,
          data: postData,
          method: "post",
          header: {
              "content-type": "application/json",
              'Authorization': 'Bearer '+ wx.getStorageSync('loginFlag')
          },
          success: function(res) {
              //返回什么就相应的做调整
              if (res.statusCode == 200) {
                  resolve(res.data);
              } else {
                  // 请求服务器成功，但是由于服务器没有数据返回，此时无code。会导致这个空数据
                  //接口后面的then执行
                  // 不下去，导致报错，所以还是要resolve下，这样起码有个返回值，
                  //不会被阻断在那里执行不下去！
                  resolve(res.data.msg);
              }
          },
          error: function(e) {
              reject("网络出错");
          }
      });
  });
  return promise;
}
// get请求封装
function getRequest(url, data) {
  var promise = new Promise((resolve, reject) => {
      var postData = data;
      wx.request({
          url: commonUrl + url,
          data: postData,
          method: "GET",
          header: {
              "content-type": "application/json",
              'Authorization': 'Bearer '+ wx.getStorageSync('loginFlag')
          },
          success: function(res) {
              if (res.statusCode == 200) {
                  resolve(res.data);
              } else {
                  resolve(res.data);
              }
          },
          error: function(e) {
              reject("网络出错");
          }
      });
  });
  return promise;
}
// DELETE请求封装
function DELETERequest(url, data) {
  var promise = new Promise((resolve, reject) => {
      var postData = data;
      wx.request({
          url: commonUrl + url+"/"+data.userAddressIds,
          data: {},
          method: "DELETE",
          header: {
              "content-type": "application/json",
              'Authorization': 'Bearer '+ wx.getStorageSync('loginFlag')
          },
          success: function(res) {
              if (res.statusCode == 200) {
                  resolve(res.data);
              } else {
                  resolve(res.data);
              }
          },
          error: function(e) {
              reject("网络出错");
          }
      });
  });
  return promise;
}
// PUT请求封装
function PUTRequest(url, data) {
    var promise = new Promise((resolve, reject) => {
        var postData = data;
        wx.request({
            url: commonUrl + url,
            data: postData,
            method: "PUT",
            header: {
                "content-type": "application/json",
                'Authorization': 'Bearer '+ wx.getStorageSync('loginFlag')
            },
            success: function(res) {
                if (res.statusCode == 200) {
                    resolve(res.data);
                } else {
                    resolve(res.data);
                }
            },
            error: function(e) {
                reject("网络出错");
            }
        });
    });
    return promise;
  }
export const post = postRequest;
export const get = getRequest;
export const DELETE = DELETERequest;
export const PUT = PUTRequest;