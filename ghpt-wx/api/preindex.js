/* 导入ip端口文件 */
import { ipConFig } from '../utils/ipconfig'

// 首页模块
const indexApi = {
  // demo
  login: () => {
    return ipConFig + '/sLogin/login'
  },
  loginByWeixin: () => {
    return ipConFig + '/sLogin/login_by_weixin'
  },
  refreshToken: () => {
    return ipConFig + '/sLogin/refreshToken'
  },
  getOrderList:() => {
    return ipConFig + '/order/list'
  }
}

module.exports = {
  indexApi
}