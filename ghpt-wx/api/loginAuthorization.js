/* 导入ip端口文件 */
import { 
  ipConFig,
  isbnApiKey,
  isbnConFig
} from '../utils/ipconfig'

// 登陆授权模块
const loginAuthorApi = {
  // 登陆
  login: () => {
    return ipConFig + 'auth/login'
  },

  // 绑定手机号
  bindPhone: () => {
    return ipConFig + '具体业务路径'
  },

  // isbn查询图书详情
  isbn: (id) => {
    return isbnConFig + id + '?apikey=' + isbnApiKey
  },
}

module.exports = {
  loginAuthorApi
}