/* 导入ip端口文件 */
import { soketConFig } from '../utils/ipconfig'

// 聊天室模块
const chatApi = {
  // 创建soket
  connectSocket: (session) => {
    return soketConFig + '业务地址/' + session
  }
}

module.exports = {
  chatApi
}