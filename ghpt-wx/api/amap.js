/* 导入ip端口文件 */
import { amapIpConFig } from '../utils/ipconfig'

// 高德地图web服务模块
const amapApi = {
  // 逆地理编码
  geocode: () => {
    return amapIpConFig + 'v3/geocode/geo'
  }
}

module.exports = {
  amapApi
}