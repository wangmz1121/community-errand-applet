// pages/address/address.js
import { get,post,PUT,DELETE} from '../../api/request'
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderListItem:[
     { addressName:'演示小区-1号楼',userName:'李云龙',userPhone:'15888888888',isFirst:true,roomNumber:'5楼'}
    ],
    falg:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    if(options.cid=='qwer'){
        this.setData({
          falg:true
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this
    let userInfo=wx.getStorageSync('userInfo')
    userInfo=JSON.parse(userInfo).userId
      get('wx/uAddr/list',{userId:userInfo.userId}).then((res)=>{
        console.log(res)
        that.setData({
          orderListItem:res.rows,
         })
      })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
    /*编辑地址 */
    edit(e){
      
      let arr=e.currentTarget.dataset.item
      arr=JSON.stringify(arr)
      wx.navigateTo({
        url: '../harvest/harvest?odj='+arr,
      })
    },
  /*新增地址 */
  newlyadded(){
    wx.navigateTo({
      url: '../harvest/harvest',
    })
  },
  /*删除*/
  onClose(event) {
    const { position, instance } = event.detail;
    switch (position) {
      case 'left':
      case 'cell':
        instance.close();
        break;
      case 'right':
        Dialog.confirm({
          message: '确定删除吗？',
        }).then(() => {
          let ayy=event.currentTarget.dataset.item
          console.log(event)
          let that=this
          DELETE('wx/uAddr',{userAddressIds :ayy.userAddressId}).then((res)=>{
            console.log(res)
            if(res.code==200){
              let userInfo=wx.getStorageSync('userInfo')
              userInfo=JSON.parse(userInfo).userId
               get('wx/uAddr/list',{userInfo:userInfo}).then((res)=>{
                  console.log(res)
                  that.setData({
                    orderListItem:res.rows,
                   })
                })
            }
          })
          // instance.close();
        }).catch(()=>{
          instance.close();
        })
        break;
    }
  },
  /*设置默认地址*/
aderssy(event){
  let ayy=event.currentTarget.dataset.item
  console.log(ayy)
  let that=this
  get('wx/uAddr/todoOne',{userAddressId:ayy.userAddressId}).then((res)=>{
    if(res.code==200){

      if(that.data.falg){
        wx.navigateBack()
      }else{
        let userInfo=wx.getStorageSync('userInfo')
         userInfo=JSON.parse(userInfo).userId
        get('wx/uAddr/list',{userInfo:userInfo}).then((res)=>{
          console.log(res)
          that.setData({
            orderListItem:res.rows,
           })
        })
      }
      Toast.success('设为默认地址');

    }
  })
},
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})