// pages/my/AboutUs/AboutUs.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
			data:{
				img:"../../static/image/index/logo2.jpg",
				title:"孤鸿跑腿",
				content:"孤鸿跑腿 , 全球生活服务领头产品 , 覆盖全球365个城市 , 业务涵盖电商，运营，社区生活等, 每日助力上千万用户，源为您提供专业安全生活服务。",
				phone:"400-890-8890转1"
			}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})