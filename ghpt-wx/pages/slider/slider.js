// pages/slider/slider.js

let startPoint;

const min = 0; // 最小宽度 单位px

const max = 300; // 最大宽度  单位px

import { pictorialIcon } from '../../mock/icon';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    buttonLeft: 0,
    progress: 0, // 进度条的宽度，这里的单位是px，所以在wxml文件中要改为rpx
    precent: 0, // 这个是百分比
    pictorialIcon
  },

  // 滑动开始
  buttonStart(e) {
    startPoint = e.touches[0];
  },
  
  // 滑动中
  moveTo(e) {
    // 防止报错，未滑动点击无效
    if(startPoint == undefined) {
      return
    }
    let endPoint = e.touches[e.touches.length - 1];
    let translateX = endPoint.clientX - startPoint.clientX;
    startPoint = endPoint;
    let buttonLeft = this.data.buttonLeft + translateX;
    if (buttonLeft > max) {
      // 滑动位置大于进度条最大宽度的时候让它为最大宽度
      buttonLeft = max;
    }
    if (buttonLeft < min) {
      // 滑动位置小于进度条最大宽度的时候让它为最小宽度
      buttonLeft = min;
    }
    let precent = parseInt((buttonLeft / max) * 100);
    this.setData({
      precent: precent,
      progress: buttonLeft,
      buttonLeft: buttonLeft
    })
  },

  // 滑动结束
  moveEnd(e) {
    console.log(e, '滑动结束')
  },

   // 保存到相册
   saveImage() {
    wx.showLoading({
      mask: true,
      title: '保存中...'
    });
    wx.downloadFile({
      url: this.data.pictorialIcon['isbn'],
      success: function(res) {
        if (res.statusCode === 200) {
          let img = res.tempFilePath;
          wx.saveImageToPhotosAlbum({
            filePath: img,
            success(res) {
              // console.log(res,'sucess')
              wx.showToast({
                icon: 'none',
                title: '保存成功'
              });
            },
            fail(err) {
              // console.log(err,'fail')
              wx.showToast({
                icon: 'none',
                title: '保存失败'
              });
            }
          });
        }
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})