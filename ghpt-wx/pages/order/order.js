// pages/home/homeLeiBo/homeLeiBo.js
import {
  request
} from '../../utils/request';
import {
  indexApi
} from '../../api/preindex';
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast'
import { get,post } from '../../api/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {    
    isClerk:false,
    value:'',
    mengban:0,
    titlearray:['','','',''],/** 搜索选项标题 */
    cdiqu: '',/** 地区选中标题*/
    cdiquindex: 0,/** 地区选中索引 */
    cjiage: '',/** 价格选中标题 */
    cjiageindex: 0,/** 价格选中索引 */
    chuxing: '',/** 户型选中标题 */
    chuxingindex: 0,/** 户型选中索引 */
    cmianji: '',/** 面积选中标题 */
    cmianjiindex: 0,/** 面积选中索引 */
    cleixing: '',/** 类型选中标题 */
    cleixingindex: 0,/** 类型选中索引 */
    showarray: [/** 展示某个搜索栏 */
      0,0,0,0
    ],
    mimg: [/**搜索页搜索框上方图片 */
      '../../static/image/index/gg2.png',
      '../../static/image/index/gg2.png'    ],
    orderListItemFdd:[],
    orderListItemFd: [],
    orderListItemYxx:[],
    orderListItemYx: [],
    homeS: [
      {
        id: 1,
        title: '进行中'
      }, {
        id: 2,
        title: '已结束'
      }
    ],
    currentInexs:0,
    currentInex:0,
    pageNum1:1,
    pageNum2:1,
    pageNum3:1,
    pageNum4:1,
    selectInex:0,
    dropList:[
      {
        id:1,
        img:"../../assets/svg/Bottom.svg",
        title:"全部"
      },
      {
        id:2,
        img:"../../assets/svg/Bottom.svg",
        title:"价格"
      },
      {
        id:3,
        img:"../../assets/svg/Bottom.svg",
        title:"户型"
      },
      {
        id:4,
        img:"../../assets/svg/Bottom.svg",
        title:"更多"
      }
    ],
    /**房东直租数据接口 */
  
    homeS:[
      {
        id:1,
        title:'进行中订单'
      },{
        id:2,
        title:'已结束订单'
      }
    ],
    diqu:[
      '黑龙江',
      '长春',
      '哈尔滨',
      '黑龙江',
      '长春',
      '哈尔滨',
      '黑龙江',
      '长春',
      '哈尔滨',
    ],
    jiage: [
      '70万以下',
      '70-80万',
      '80-100万',
      '100-120万',
      '120-140万',
      '140-160万',
    ],
    huxing:[
      '1室',
      '2室',
      '3室',
      '4室',
      '5室',
      '5室以上'
    ],
    mianji:[
      '50以下',
      '50-70',
      '70-90',
      '90-110',
      '110以上',

    ],
    leixing:[
      '住宅',
      '别墅',
      '商铺',
      '写字楼',
      '其他',
    ]
  },
  onTaps:function(e){
    var nums= e.currentTarget.dataset.index;
    /**更新赋值 setData() */
    this.setData({
      showarray:[0,0,0,0],
      mengban:'0',
      titlearray:['','','',''],
      currentInexs:nums,
      /**数据源赋值 */
    });
    console.log(this.data.currentInexs)
   },
   /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.id == null){
      options.id = 0;
    }

    let userInfo=wx.getStorageSync('userInfo')
    userInfo=JSON.parse(userInfo).userId
    let that=this
    //查询已完成订单列表
    get('wx/order/listEnd',{personId:userInfo,pageNum:1,pageSize:5}).then((res)=>{
      console.log(res)
      that.data.pageNum2++
      that.setData({
        orderListItemYx:res.rows,
        orderListItemYxx:res.rows
       })
    })
    //查询进行中订单列表
    get('wx/order/listRun',{personId:userInfo,pageNum:1,pageSize:5}).then((res)=>{
      console.log(res)
      that.data.pageNum1++
      that.setData({
        orderListItemFd:res.rows,
        orderListItemFdd:res.rows,
      })
    })
     this.setData({
       currentInexs:options.id,
       isClerk:wx.getStorageSync('isClerk')
     })
    //  const res = {
    //   method: 'get',
    //   data: {
    //     pageNum: 1,
    //     pageSize: 10,
    //     communityId: null,
    //     personId: null,
    //     busType: null,
    //     pickCode: null,
    //     personName: null,
    //     totalPrice: null,
    //     payPrice: null,
    //     freightPrice: null,
    //     state: null,
    //   },
      // data: {
      //   data:wx.getStorageSync('userInfo')
      // },
    //   url: indexApi.getOrderList()
    // };
   },
   /*搜索*/
   onChange(e) {
    this.setData({
      value: e.detail,
    });
    if(this.data.value==''){
      this.data.pageNum3=1
      this.data.pageNum4=1
      this.setData({
        orderListItemFd:this.data.orderListItemFdd,
        orderListItemYx:this.data.orderListItemYx,
      })
    }
  },
  onClick() {
    
    let userInfo=wx.getStorageSync('userInfo')
    userInfo=JSON.parse(userInfo).userId
    let that=this
    let addlist=JSON.stringify(that.data.value)
    console.log(addlist)
    if(this.data.currentInexs==0){
    //搜索进行中订单列表
    that.setData({
      pageNum3:1,
    })
    get('wx/order/listRun',{personId:userInfo,pageNum: that.data.pageNum3,pageSize:5,pickCode:that.data.value}).then((res)=>{
      console.log(res)
      that.data.pageNum3++
      that.setData({
        orderListItemFd:res.rows,
      })
    })
    }else{
      that.setData({
        pageNum4:1,
      })
        //搜索已完成订单列表
    get('wx/order/listEnd',{personId:userInfo,pageNum: that.data.pageNum4,pageSize:5,pickCode:that.data.value}).then((res)=>{
      console.log(res)
      that.data.pageNum4++
      that.setData({
        orderListItemYx:res.rows,
       })
    })
    }
  },
  /**导航标题联动 e是什么？log打印看看*/
  selectToggle:function(e){
    var num = e.currentTarget.dataset.index;
    this.setData({ showarray: [0, 0, 0, 0] ,
    mengban:1});
    if (num == 0) {
      this.setData({
        showarray: [1, 0, 0, 0]
      })
    } else if (num == 1) {
      this.setData({
        showarray: [0, 1, 0, 0]
      })
    } else if (num == 2) {
      this.setData({
        showarray: [0, 0, 1, 0]
      })
    } else if (num == 3) {
      this.setData({
        showarray: [0, 0, 0, 1]
      })
    };
    console.log(this.data.showarray);
   
   /**更新赋值 setData() */
   this.setData({
     currentInex:num
   }) 
  },
  /**选项选中 */
  select:function(e){
    var num = e.currentTarget.dataset.index;
    var type = e.currentTarget.dataset.type;
    if(type==0){
      this.setData({
        cdiquindex:num,
      })
    }else if(type==1){
        this.setData({
          cjiageindex:num,
        })
    } else if (type == 2) {
      this.setData({
        chuxingindex: num,
      })
    } else if (type == 3) {
      this.setData({
        cmianjiindex: num,
      })
    } else if (type == 4) {
      this.setData({
        cleixingindex: num,
      })
    }

  },
  /**地区确定 */
  diquok:function(e){
    this.setData({
      mengban: 0,
      cdiqu: this.data.diqu[this.data.cdiquindex]
    })
    this.setData({
      titlearray:[this.data.cdiqu,this.data.cjiage,this.data.chuxing,this.data.cmianji],
      showarray:[0,0,0,0]
    })
  },
  /**价格确定 */
  jiageok:function(e){
    this.setData({
      mengban: 0,
      cjiage: this.data.jiage[this.data.cjiageindex],
    })
    this.setData({
      
      titlearray: [this.data.cdiqu, this.data.cjiage, this.data.chuxing, 
      this.data.cmianji],
      showarray: [0, 0, 0, 0]
    })
    
  },
  /**户型确定 */
  huxingok:function(e){
    this.setData({
      mengban: 0,
      chuxing: this.data.huxing[this.data.chuxingindex],
    })
    this.setData({
      
      titlearray: [this.data.cdiqu, this.data.cjiage, this.data.chuxing, this.data.cmianji],
      showarray: [0, 0, 0, 0]
    })
    
  },

  /**更多确定 */
  gengduook:function(e){
    this.setData({
      mengban: 0,
      cmianji: this.data.mianji[this.data.cmianjiindex],
      cleixing: this.data.leixing[this.data.cleixingindex],
    })
    if(this.data.cmianji == null){
      this.setData({
        cmianji:''
      })
    }
    this.setData({
      titlearray: [this.data.cdiqu, this.data.cjiage, this.data.chuxing, this.data.cmianji],
      showarray: [0, 0, 0, 0]
    })
    
  },
  gotolistSellBuy:function(e){
    wx.navigateTo({
      url: '/pages/home/listSellBuy/listSellBuy'
    })
  },

  
  mengbanok:function(){
    this.setData({
      showarray:[0,0,0,0],
      mengban:'0',
    })
  },
  buxian:function(){
    this.setData({
      chuxingindex:-1,
      showarray: [0, 0, 0, 0],
      mengban: '0',
      chuxing:''
    })
    this.setData({
      titlearray:[this.data.cdiqu,this.data.cjiage,this.data.chuxing,this.data.cmianji]
    })
  },
  chongzhi: function () {
    this.setData({
      cmianjiindex: -1,
      cleixingindex: -1,
      showarray: [0, 0, 0, 0],
      mengban: '0',
      cmianji: '',
      cleixing:''
    })
    this.setData({
      titlearray: [this.data.cdiqu, this.data.cjiage, this.data.chuxing, this.data.cmianji]
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let userInfo=wx.getStorageSync('userInfo')
    userInfo=JSON.parse(userInfo).userId
    let that=this
    if(this.data.currentInexs==0){
      if(this.data.value==''){
           //查询进行中订单列表
          get('wx/order/listRun',{personId:userInfo,pageNum:that.data.pageNum1,pageSize:5}).then((res)=>{
            console.log(res)
          if(that.data.orderListItemFd.length>=res.total){
            Toast('没有更多订单了');
              return
          }
            for(let temp of res.rows){
              that.data.orderListItemFd.push(temp)
              that.data.orderListItemFdd.push(temp)
            }
            that.data.pageNum1++
            that.setData({
              orderListItemFd:that.data.orderListItemFd,
            })
          })
      }else{
        //查询进行中订单列表
        get('wx/order/listRun',{personId:userInfo,pageNum:that.data.pageNum3,pageSize:5,pickCode:that.data.value}).then((res)=>{
          console.log(res)
        if(that.data.orderListItemFd.length>=res.total){
          Toast('没有更多订单了');
            return
        }
          for(let temp of res.rows){
            that.data.orderListItemFd.push(temp)
          }
          that.data.pageNum3++
          that.setData({
            orderListItemFd:that.data.orderListItemFd,
          })
        })
      }
     
    }else{
      if(this.data.value==''){
      //查询已完成订单列表
      get('wx/order/listEnd',{personId:userInfo,pageNum:that.data.pageNum2,pageSize:5}).then((res)=>{
        console.log(res)
        if(that.data.orderListItemYx.length>res.total){
          Toast('没有更多订单了');
          return
       }
        for(let temp of res.rows){
          that.data.orderListItemYx.push(temp)
          that.data.orderListItemYxx.push(temp)
        }
        that.data.pageNum2++
        that.setData({
          orderListItemYx:that.data.orderListItemYx,
         })
      })
    }else{
      get('wx/order/listEnd',{personId:userInfo,pageNum:that.data.pageNum4,pageSize:5,pickCode:that.data.value}).then((res)=>{
        console.log(res)
        if(that.data.orderListItemYx.length>res.total){
          Toast('没有更多订单了');
          return
       }
        for(let temp of res.rows){
          that.data.orderListItemYx.push(temp)
        }
        that.data.pageNum4++
        that.setData({
          orderListItemYx:that.data.orderListItemYx,
         })
      })
    }
  }
    console.log(that.data.orderListItemFd)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})