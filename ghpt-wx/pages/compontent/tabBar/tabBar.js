// pages/compontent/tabbar/tabbar.js

import { tabbarIcon } from "../../../mock/icon";

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    middleIcon: String
  },

  options: {
    // 子组件样式不起作用的解决办法(必填)
    addGlobalClass: true,
    // 启用插槽
    multipleSlots: true
  },

  /**
   * 组件的初始数据
   */
  data: {
    tabbarIcon,
    transitionMiddleImg: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 底部栏跳转
    tabBarTap(e) {
      const url = e.currentTarget.dataset.url
      wx.redirectTo({
        url: '/pages/' + url + '/' + url
      })
    },

    // 中间icon加载完成
    bindMiddleImgLoad(e) {
      this.setData({
        transitionMiddleImg: 'transition-middle-img'
      })
    }
  },

  // 在组件实例进入页面节点树时执行
  attached: function() {
    
  }
})