import { post,DELETE,get } from "../../../api/request";
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'
// components/orderListItem/orderListItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    orderListItem:{
      type:Array,
      value:[],
      show: false,
      listdata:{},
      isClerk:false,
      
    },
    homeFeuer:{
      type:Array,
      value:[]
    },
    
  },

  /**
   * 组件的初始数据
   */
  data: {
    state:0,
    isClerk:false,
    active :0,
    steps: [
      {
        text: '待接单'
      },
      {
        text: '待取货'
      },
      {
        text: '待收货'
      },
      {
        text: '已收货'
      },
    ],
    maifangImg:[
          {
            img:'../../static/image/index/qkd.png',
            title:'取快递',
            busType:'1'
          },
          {
            img:'../../static/image/index/qwm.png',
            title:'取外卖',
            busType:'2'
          },
          {
            img:'../../static/image/index/qtg.png',
            title:'取团购',
            busType:'3'
          },
          {
            img:'../../static/image/index/bmbs.png',
            title:'帮买帮送',
            busType:'4'
          }
        ]
        
  },
  ready(){
    for(let temp of this.data.orderListItem){
      for(let item of this.data.maifangImg){
          if( temp.busType==item.busType){
             temp.datalist=item.title
          }
      }
    }
    this.setData({
      orderListItem:this.data.orderListItem
    })
    console.log(this.data.orderListItem)
  },
  /**
   * 组件的方法列表
   */
  methods: {
		detail(e){
       
      console.log(this.data.steps)
			// wx.navigateTo({
			// 	url:'/pages/order/personalHousing/personalHousing?id='+e.currentTarget.dataset.index
      // })
      this.setData({ listdata: e.currentTarget.dataset.item});
      this.setData({ show: true });
       let  a=wx.getStorageSync('isClerk');
      this.setData({isClerk:a});
      this.setData({state:e.currentTarget.dataset.item.state});

		},
    gotoLeiBo:function(e){
      wx.navigateTo({
        url: '/pages/order/order?id=0'
      })
    },
    showPopup() {
      this.setData({ show: true 
      });
    },
  
    onSClose() {
      this.setData({ show: false });
    },
    dist(e){
      console.log(e)
    },
    nextspet(){
       
        let orderId=this.data.listdata.orderId
        console.log(orderId)
        let that=this
        get('wx/order/todoOrder',{orderId:orderId}).then((res)=>{
          if(res.code==200){
            Toast({
              type: 'success',
              message: '处理成功',
              onClose: () => {
                this.setData({ show: false });
                wx.navigateTo({
                  url: '/pages/order/order?id=0'
                })
              },
            });
          }
        })
    },
    delt(){
        let orderId=this.data.listdata.orderId
        get('wx/order/endOrder',{orderId:orderId}).then((res)=>{
          if(res.code==200){
            Toast({
              type: 'success',
              message: '取消成功',
              onClose: () => {
                this.setData({ show: false });
                wx.navigateTo({
                  url: '/pages/order/order'
                })
              },
            });
           
          }
        })
    }
  },

})
