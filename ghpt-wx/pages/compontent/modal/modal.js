// pages/compontent/modal/modal.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    modalName: {
      type: String,
      value: ''
    },
    modalWidth: {
      type: String,
      value: '60'
    },
    modalType:　{
      type: String,
      value: 'center'
    }
    // modalClass
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  options: {
    // 子组件样式不起作用的解决办法(必填)
    addGlobalClass: true,
    // 启用插槽
    multipleSlots: true
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 点击遮罩层隐藏弹框事件
    hideMask() {
      this.triggerEvent("hideMask");
    }
  }
})
