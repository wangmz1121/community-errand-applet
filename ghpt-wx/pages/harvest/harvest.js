// pages/harvest/harvest.js
import { get,post,PUT } from '../../api/request'
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userAddressId:'',
    isFirst:0,
    sex:0,
    userId:'',
    userName:'',
    userPhone:'',
    addressNamea:'',
    roomNumber:'',
    xiu:'保存地址'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.odj!=undefined){
      let obj=JSON.parse(options.odj)
        this.setData({
          userName:obj.userName,
          userPhone:obj.userPhone,
          addressNamea:obj.addressName,
          roomNumber:obj.roomNumber,
          userAddressId:obj.userAddressId,
          isFirst:obj.isFirst,
          xiu:'修改地址'
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
/*保存和修改地址*/
newlyadded(){
  let userInfo=wx.getStorageSync('userInfo')
  userInfo=JSON.parse(userInfo).userId
  if(this.data.xiu=='保存地址'){
    let arr={
      userId:userInfo,
      userName:this.data.userName,
      userPhone:this.data.userPhone,
      addressName:this.data.addressNamea,
      roomNumber:this.data.roomNumber,
      sex:this.data.sex,
      isFirst:1
    }
    //post新增  put修改
    post('wx/uAddr',arr).then((res)=>{
      console.log(res)
      Toast.success('修改成功');
      wx.navigateBack()
    })
  }else{
    let arr={
      userAddressId:this.data.userAddressId,
      isFirst:this.data.isFirst,
      userId:userInfo,
      userName:this.data.userName,
      userPhone:this.data.userPhone,
      addressName:this.data.addressNamea,
      roomNumber:this.data.roomNumber,
      sex:this.data.sex
    }
    PUT('wx/uAddr',arr).then((res)=>{
      Toast.success('保存成功');
      wx.navigateBack()
    })
  }
},
/*修改默认*/
onChange() {
  if(this.data.isFirst==1){
    this.setData({
       isFirst: 0,
    });
  }else{
    this.setData({
      isFirst: 1,
     });
  }
},
onChange1(){
  if(this.data.sex==1){
    this.setData({
      sex: 0,
    });
  }else{
    this.setData({
      sex: 1,
     });
  }
},
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})