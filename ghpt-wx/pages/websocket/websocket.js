// pages/websocket/websocket.js

const app = getApp();

const util = require('../../utils/util');

import { otherIcon } from '../../mock/icon';

import { webSocket } from '../../mock/other';

// import { request } from '../../utils/request';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    headerHeight: 0,
    // 商品id
    goods:{
      imgSrc: otherIcon.goodsImg,
      price: '100',
      description: '哆啦A梦'
    },
    // 用户信息
    user: {
      // id
      id: 299,
      // 昵称
      nickname: '一缕阳光'
    },
    // 输入框值
    inputValue: '',
    // 当前页数
    pageNum: 1,
    // 最大页数
    pageTotal: 1,
    // 分页到底提示
    prompt: '',
    // 列表数据
    listData: [],
    // 没有更多
    noMore: false,
    // 下拉刷新加载更多
    refreshShow: false,
    // 我的头像
    myPortrait: webSocket.myPortrait,
    // 其他头像
    otherPortrait: webSocket.otherPortrait
  },

  // 输入框输入时
  inputConfirm(e) {
    this.setData({
      inputValue: e.detail.value
    })
  },

  // listdata时间处理
  dataMap() {
    let newArray = [
      {
        content: "我想要这个哆啦A梦",
        createTime: "2021-07-08 21:21:11",
        objectId: "60e6fbc7b7305501fc46b7f9",
        status: 1,
        userFrom: 299,
        userTo: 298,
      },
      {
        content: '请问我们在哪见面交易？',
        createTime: '2021-07-08 21:21:44',
        objectId: '60e6fbe8b7305501fc46b7fa',
        status: 1,
        userFrom: 299,
        userTo: 298,
      },
      {
        content: '华师大中北校区2号楼吧',
        createTime: '2021-08-21 21:39:33',
        objectId: '6121021563dba620e51b2c5f',
        status: 1,
        userFrom: 298,
        userTo: 299
      },
      {
        content: '拼这个很辛苦的...&#128148;',
        createTime: '2021-08-22 16:21:55',
        objectId: '61220923ab7f0c43c912565e',
        status: 1,
        userFrom: 298,
        userTo: 299,
      },
    ]
    newArray.map(item => {
      // 灰色时间差显示
      item.dateTimeShow = false;
      // 表情解码 &#128148; => 💔  表情转unicode码
      item.content = util.uncodUtf16(item.content);
      // 新增一个格式化后的字段保存返回结果
      item.newDateTime = util.dateUtils.format(item.createTime);
    })
    // 当前数据和下一跳数据进行时间差比较，2条消息发送时间差超过2分钟以上显示灰色时间提示
    newArray.reduce((prev, cur) => {
      const prevTime = parseInt(prev.newDateTime)
      const curTime = parseInt(cur.newDateTime)
      if((prevTime - curTime) > 2) {
        cur.dateTimeShow = true;
      }
      return cur
    })
    this.setData({
      listData: newArray
    })
  },

  // socket通信
  onSocketOpen() {
    // Socket 接受到服务器的消息事件
    wx.onSocketMessage(res => {
      if(res.data !== 'connect successfully') {
        // websocket推送消息是字符串需要json.pase转一下
        const data = JSON.parse(res.data);
        // 消息推送从底部插入，并且页面置地
        this.data.listData.push(data);
        this.setData({
          listData: this.data.listData
        })
        this.scrollToBottom();
      }
    })
  },

  // 发送消息
  sendMessage() {
    if(this.data.inputValue === '') {
      return wx.showToast({
        title: '不能发送空白消息!',
        icon: 'none'
      })
    }
    // 此处做演示，手动渲染页面
    this.data.listData.push({
      // utf16toEntities => 输入内容如果有表情转码
      content: util.utf16toEntities(this.data.inputValue),
      createTime: "2022-01-26 15:20:01",
      objectId: "60e6fbc7b7305501fc46b7f9",
      status: 1,
      userFrom: 299,
      userTo: 298,
    })
     this.data.listData.push({
      content: "ok哟，我是自动回复，收到啦！",
      createTime: "2022-01-26 15:21:01",
      objectId: "60e6fbc7b7305501fc46b7f9",
      status: 1,
      userFrom: 298,
      userTo: 299,
    })
    this.data.listData.map(item => {
      // 灰色时间差显示
      item.dateTimeShow = false;
      // 表情解码 &#128076; => 👌  表情转unicode码
      item.content = util.uncodUtf16(item.content);
      // 新增一个格式化后的字段保存返回结果
      item.newDateTime = util.dateUtils.format(item.createTime);
    })
    // 当前数据和下一跳数据进行时间差比较，2条消息发送时间差超过2分钟以上显示灰色时间提示
    this.data.listData.reduce((prev, cur) => {
      const prevTime = parseInt(prev.newDateTime)
      const curTime = parseInt(cur.newDateTime)
      if((prevTime - curTime) > 2) {
        cur.dateTimeShow = true;
      }
      return cur
    })
    this.setData({
      listData: this.data.listData
    })
    // 有后台可使用下面部分
    // const that = this;
    // 发送消息参数格式，根据后台规定来定义，此处只做演示
    // const data = {
    //   content: this.data.inputValue,
    //   userTo: this.data.user.id,
    //   userFrom: wx.getStorageSync('userId')
    // }
    // socket发送消息
    // wx.sendSocketMessage({
    //   data: JSON.stringify(data),
    //   success: async function () {
    //     // 开始加载等待
    //     wx.showLoading({
    //       mask: true,
    //       title: '加载中'
    //     })
    //     // 结束loading
    //     wx.hideLoading();
    //   },
    //   fail: function (e) {
    //     console.log(e)
    //   },
    //   complete: function () {
    //     // 延迟执行置地函数
    //     setTimeout(() => {
    //       that.scrollToBottom();
    //     }, 100)
    //     that.setData({
    //       inputValue: '',
    //       listData: that.data.listData
    //     })
    //   }
    // });
  },

  // 我想要
  async iWantTo() {
    wx.showToast({
      title: "发起支付",
      icon: 'none',
      duration: 3000
    })
    setTimeout(() => {
      wx.showToast({
        title: "支付成功, 记得和卖家沟通取货哦~",
        icon: 'none',
        duration: 5000
      })
    }, 3000);
    // 吊接口获取支付apix需要的参数然后调用支付函数
    // this.requestPayment()
  },

  // 微信支付
  requestPayment(res) {
    wx.requestPayment({
      'timeStamp': '',
      'nonceStr': '',
      'package': '',
      'signType': '',
      'paySign': '',
      'success': function(res) {
        // console.log(res);        
      },
      'fail': function(res) {
        // console.log(res)
      }
    })
  },

  // 置底
  scrollToBottom() {
    wx.pageScrollTo({
      scrollTop: 999,
      duration: 100
    })
  },

  // 下拉刷新
  pullRefresh() {
    // 开始加载等待
    wx.showLoading({
      mask: true,
      title: '加载中'
    })
    setTimeout(() => {
      if(this.data.refreshShow) {
        this.setData({
          prompt: '没有更多消息了',
          noMore: true,
        })
      } else {
        this.data.listData.unshift({
          content: "你好哟,小可爱",
          createTime: "2021-07-08 21:11:01",
          objectId: "60e6fbc7b7305501fc46b7f9",
          status: 1,
          userFrom: 298,
          userTo: 299,
        })
        this.data.listData.unshift({
          content: "你好哟,小姐姐",
          createTime: "2021-07-08 21:09:13",
          objectId: "60e6fbc7b7305501fc46b7f9",
          status: 1,
          userFrom: 299,
          userTo: 298,
        })
        this.data.listData.map(item => {
          // 灰色时间差显示
          item.dateTimeShow = false;
          // 表情解码 &#128148; => 💔  表情转unicode码
          item.content = util.uncodUtf16(item.content);
          // 新增一个格式化后的字段保存返回结果
          item.newDateTime = util.dateUtils.format(item.createTime);
        })
        // 当前数据和下一跳数据进行时间差比较，2条消息发送时间差超过2分钟以上显示灰色时间提示
        this.data.listData.reduce((prev, cur) => {
          const prevTime = parseInt(prev.newDateTime)
          const curTime = parseInt(cur.newDateTime)
          if((prevTime - curTime) > 2) {
            cur.dateTimeShow = true;
          }
          return cur
        })
        this.setData({
          refreshShow: true,
          listData: this.data.listData
        })
      }
      // 结束loading
      wx.hideLoading();
      // 通知页面停止下拉刷新效果
      wx.stopPullDownRefresh();
    }, 1000);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      headerHeight: app.globalData.CustomBar
    })
    this.dataMap();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 监听socket推送事件
    // this.onSocketOpen();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {  
    this.pullRefresh();    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})