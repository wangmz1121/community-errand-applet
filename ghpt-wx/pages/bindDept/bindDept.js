// pages/bindDept/bindDept.js
import { get,post } from '../../api/request'
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    radio: '1',
    deptList:[
      {
        img:'/assets/home/homeIcon/01.png',
        title:'取快递',
        id:'1212'
      }
    ]
  },

  onChange(event) {
    this.setData({
      radio: event.detail,
    });
  },

  onClick(event) {
    const { name } = event.currentTarget.dataset;
    this.setData({
      radio: name,
    });
    get('wx/dept/bindDept',{deptId:name}).then((res)=>{
      Toast({
        type: 'fail',
        message: '绑定小区成功，将自动返回主页',
        onClose: () => {
          wx.navigateTo({
            url: '/pages/home/home'
          })
        },
      });
      wx.setStorageSync('bindDept',true)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    get('wx/dept/list',{ }).then((res)=>{
      console.log(res)
      this.setData({
        deptList:res.rows,
       })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})