// pages/index/error/error.js

import { otherIcon } from "../../mock/icon";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    otherIcon,
    // 接口地址
    url: '',
    // 接口参数
    data: '',
    errno: '',
    errmsg: '',
    // code
    successCode: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const url = options.url?(options.url.split("5679/")[1] || options.url.split("//")[1]):"未知路径";
    const time = Date.now();
    const version = wx.getAccountInfoSync().miniProgram.version || '开发版'
    this.setData({
      version,
      time: time,
      url: url,
      type: options.type,
      errno: options.errno,
      errmsg: options.errmsg,
      successCode: options.successCode
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})