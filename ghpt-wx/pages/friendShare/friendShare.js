// pages/friendShare/friendShare.js

const app = getApp();

// import { indexApi } from '../../../api/index';

// import { request } from '../../../utils/request';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 自定义头部主页
    isHome: false,
    // 自定义头部返回
    isBack: false
  },

  // 分享优惠劵跳转头部栏显示主页非返回按钮
  customIcon(message) {
    if(message === 'message') {
      this.data.isHome = true;
    } else {
      this.data.isBack = true;
    }
    this.setData({
      isHome: this.data.isHome,
      isBack: this.data.isBack,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    // 开启右上角分享好友功能
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage']
    })
    await this.customIcon(options.type);
    const token = wx.getStorageSync('token');
    // 分享内容点击进入判断token是否存在
    if (!token && this.data.isHome) {
      // 接受到通知已经获取到token后在执行接口
      app.tokenCallback = async(token) => {
        if (token) {
          console.log('tokenCallback')
        }
      }
    } else {
      /*  
        * 第一次判断token接口是否完成，
        * 后续不需做判断直接执行接口。
      */ 
      console.log('else')
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: async function (res) {
    let title = '分享好友 / 群';
    let path = 'pages/friendShare/friendShare?id=' + '123445678' + '&type=message';
    if (res.from === 'button') {
      // 来自页面内转发按钮
      const dataset = res.target.dataset;
      // 超过10个字符长度截取
      if (dataset.title.length > 10) {
        title = dataset.title.substring(0, 10) + '...'
      }
      title = dataset.title;
      path = 'pages/friendShare/friendShare?id=' + dataset.id + '&type=message'
    }
    return {
      title,
      path
    }
  }
})