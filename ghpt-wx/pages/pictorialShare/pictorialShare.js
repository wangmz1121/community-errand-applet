// pages/pictorialShare/pictorialShare.js

// 开发者工具会报 TypeError: Cannot read property 'map' of undefined 可以无视，不影响正常功能使用和发布。
// 真机调试使用1.0版本，2.0版本不稳定，有几率报错不显示

import { pictorialIcon } from '../../mock/icon';

// 引入 `wx-canvas-2d` 插件

import {
  Text,
  Rect,
  Image,
  WxCanvas2d,
  SaveToAlbum
} from '../../wx-canvas-2d/index';

// 创建画布
WxCanvas2d.use(SaveToAlbum);

const canvas = new WxCanvas2d();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pictorialIcon,
    canvas: null,
    style:{
      name:"闲置商品",
      bgColor: "#FAD0C4",
      titleMaxWidth: 160,
      titleKey: "华师小镇", //标题索引
      descKey: "二手交易市场",
      iconConfig: {  //标题后面的符号配置
          type: Text,
          text: '￥',
          width: 180,
          ellipsis: 1,
          x: 235,
          y: 392,
          zIndex: 1,
          fontWeight: '600',
          color: "#E62E4D",
          fontSize: 16
      },
      extraKey: "0.3",
      extraConfig: {
        type: Text,
        text: "",
        width: 180,
        ellipsis: 1,
        x: 250,
        y: 389,
        zIndex: 1,
        fontWeight: '600',
        color: "#E62E4D",
        fontSize: 22
      }
    }
  },

  // 保存到相册
  savePoster(){
    wx.showLoading();
    this.data.canvas.save({
      destWidth: 680,
      destHeight: 1200
    }).then(() => {
      wx.hideLoading();
      wx.showToast({
        icon: 'none',
        duration: 3000,
        title: '下载成功~ 请手动在朋友圈分享哟~ 截图给助手小姐姐有神秘惊喜哟~ ~'
      })
    })
    .catch(()=>{
      wx.hideLoading();
      wx.showToast({
        icon: 'none',
        duration: 3000,
        title: '下载失败~ 请重试'
      })
    })
  },

  // 创建cavans画布
  createCanvas() {
    canvas.create({
      query: '#poster-canvas', // 必传，canvas元素的查询条件
      rootWidth: 385, // 参考设备宽度 (即开发时UI设计稿的宽度，默认375，可改为750)
      bgColor: this.data.style.bgColor, // 背景色，默认透明
      component: this, // 自定义组件内需要传 this
      radius: 10 // 海报图圆角，如果不需要可不填
    })

    // 清空画布以防内容重叠
    canvas.draw({});
    // 赋值新内容
    canvas.draw({
      series: [
        // 用户头像
        {
          type: Image,
          url: this.data.pictorialIcon['headPortrait'],
          x: 24,
          y: 24,
          width: 60,
          height: 60,
          mode: 'aspectFill',
          radius: 4,
          zIndex: 0
        },
        // 用户昵称
        {
          type: Text,
          text: '小镇助手',
          width: 234,
          ellipsis: 1,
          x: 93,
          y: 26,
          fontSize: 15,
          fontWeight: '500',
          color: "#353535"
        },
        {
          type: Text,
          text: 'Hi，我在华师小镇发布了闲置商品，快来关注吧~',
          x: 93,
          y: 52,
          width: 234,
          ellipsis: 2,
          fontSize: 14,
          color: '#353535',
          lineHeight: 14 * 1.2,
        },
        {
          type: Rect,
          x: 24,
          y: 100,
          width: 303,
          height: 398,
          bgColor: '#fff',
          radius: 4,
          lineStyle: {
            color: '#fff',
            width: 1
          },
          zIndex: 1
        },
        // 商品图片
        {
          type: Image,
          url: this.data.pictorialIcon['qrCode'],
          x: 56,
          y: 132,
          width: 239,
          height: 239,
          mode: 'aspectFill',
          radius: 4,
          zIndex: 2
        },
        // 商品标签
        {
          type: Text,
          text: this.data.style.titleKey,
          width: this.data.style.titleMaxWidth,
          ellipsis: 1,
          x: 56,
          y: 389,
          zIndex: 1,
          fontWeight: '600',
          color: "#353535",
          fontSize: 20
        },
        { //符号配置
          ...this.data.style.iconConfig
        },
        // 补充信息配置
        {
          ...this.data.style.extraConfig,  //加载预设配置
          text: this.data.style.extraKey  //重写text内容
        },
        // 商品备注
        {
          type: Text,
          text: this.data.style.descKey,
          x: 56,
          y: 426,
          width: 234,
          ellipsis: 2,
          fontSize: 16,
          color: '#353535',
          lineHeight: 16 * 1.2,
          zIndex: 1
        },
        // 二维码图片
        {
          type: Image,
          url: this.data.pictorialIcon['qrCode'],
          x: 56,
          y: 514,
          width: 80,
          height: 80,
          mode: 'aspectFill',
          radius: 40,
          zIndex: 2
        },
        {
          type: Text,
          text: '长按小程序码，了解详情',
          x: 157,
          y: 546,
          fontSize: 12,
          fontWeight: '400',
          color: '#353535',
          zIndex: 1
        }
      ]
    })
    this.setData({
      canvas
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.createCanvas();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})