// pages/publish/buy/buy.js
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
import { get,post,PUT } from '../../api/request'
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';

Page({

  /**
   * 页面的初始数据
   */
  data: {
		img:["../../static/image/index/gg.png"],
		name:"",
		number1:"",
		currentTypeChoose:0,
		currentFitmentChoose:0,
    currentSpecificationChoose:0,
    uAddr:[],
    addrFlag :false,
    busType:'',
    pickCode:'',
    pickAddr:'',
    hopeTime:'立刻',
    remark:'',
		type:[
      {
        id:'1',
        name:	"取快递"
      },
      {
        id:'2',
        name:	"取外卖"
      },
      {
        id:'3',
        name:	"取团购"
      },
      {
        id:'4',
        name:	"帮买帮送"
      }
		],
		fitment:[
      {
        addressId:'4',
        addressName:	"南门口菜鸟驿站"
      },
      {
        addressId:'4',
        addressName:	"北门口蜂巢快递柜"
      },
      {
        addressId:'4',
        addressName:	"西门口"
      }
		],
		specification:[
			"立刻",
      "12:00-14:00",
      "18:00后",
      "其他"
		],
		show: false,
    deptAddrList:[],
    orderListItem:[],
    showlit:false,
    timelist:[{name:'8:00-10:00'},{name:'10:00-12:00'},{name:'12:00-14:00'},{name:'14:00-16:00'},{name:'16:00-18:00'},{name:'18:00-20:00'},{name:'20:00-22:00'}]
  },
  cancel() {
    this.setData({ showlit: false });
  },

  onSelect(event) {
    console.log(event.detail);
    this.data.specification=[
			"立刻",
      "12:00-14:00",
      "18:00后",
      event.detail.name
		],
    this.setData({ showlit: false,specification:this.data.specification ,hopeTime:event.detail.name});
  },
  onChange(event) {
    // event.detail 为当前输入的值
    // this.data.pickCode=event.detail.value;
    this.data.pickCode=event.detail;
  },
  onChange2(event) {
    // this.data.remark=event.detail.value;
    this.data.remark=event.detail;
  },
	change(e){
		// 判断是否登录
		// let token=wx.getStorageSync("token");
		// if(!token){
		// 	wx.showModal({
		// 		title:"请先登录",
		// 		content:"仅对注册用户开放。"
		// 	})
		// 	return
		// }
		console.log(e)
	},
	change1(e){
		// let token=wx.getStorageSync("token");
		// if(!token){
		// 	wx.showModal({
		// 		title:"请先登录",
		// 		content:"仅对注册用户开放。"
		// 	})
		// 	return
		// }
		console.log(e)
	},
	publish(){
		let that=this;
		let reg = /^[\u4E00-\u9FA5]+$/;
		if (that.data.pickCode =='' && that.data.busType==1) {
			wx.showToast({
				title: '请输入取件码',
				icon: "none"
			})
			return
    }
    if (that.data.remark =='' && that.data.busType==4) {
			wx.showToast({
				title: '请输入任务描述',
				icon: "none"
			})
			return
		}
		// if(that.data.uAddr.userPhone.length<11){
		// 	wx.showToast({
		// 		title: '请输入正确手机号',
		// 		icon: "none"
		// 	})
		// 	return
    // };
    let userInfo=wx.getStorageSync('userInfo')
		userInfo=JSON.parse(userInfo).userId

	  let order={
      userId:userInfo,
      pickCode:that.data.pickCode,
      remark:that.data.remark,
      hopeTime:that.data.hopeTime,
      pickAddr:that.data.pickAddr,
      personName:this.data.uAddr.userName ,
      userPhone:this.data.uAddr.userPhone,
      userAddr:this.data.uAddr.addressName + this.data.uAddr.roomNumber,
      busType:that.data.busType,
      sex:this.data.uAddr.sex,
    }
    
    //post新增  put修改
    post('wx/order/wxAdd',order).then((res)=>{
      Toast({
        type: 'success',
        message: '发布成功！',
        onClose: () => {
          wx.navigateTo({
            url: '/pages/home/home'
          })
        },
      });
      
    })
	},
	type(e){
		this.setData({
			currentTypeChoose:e.currentTarget.dataset.index
		})	
	},
	fitment(e){
		
		this.setData({
      currentFitmentChoose:e.currentTarget.dataset.index,
      pickAddr:e.currentTarget.dataset.word
		})	
	},
	radio(){
		if(this.data.uAddr.sex==1){
			this.setData({
				['uAddr.sex']:0
			})
		}else{
			this.setData({
				['uAddr.sex']:1
			})
		}
	
	},
	specification(e){
    console.log(e)
    if(e.currentTarget.dataset.index==3){
      this.setData({
       showlit:true
      })
    }
		this.setData({
      currentSpecificationChoose:e.currentTarget.dataset.index,
      hopeTime:e.currentTarget.dataset.word
		})
	},
	name(e){
		this.setData({
			name:e.detail.value
		})
	},
	number1(e){
		this.setData({
			number1:e.detail.value
		})
	},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let a =wx.getStorageSync('bindDept');
    if(a ==''){
      Toast({
        type: 'fail',
        message: '授权失败，请重新授权或联系管理员',
        onClose: () => {
          wx.navigateTo({
            url: '/pages/home/home'
          })
        },
      });
    }
    if(!wx.getStorageSync('bindDept')){
      

      Dialog.confirm({
        title: '',
        message: '请绑定您所在的小区',
      })
        .then(() => {
          wx.navigateTo({
            url: '/pages/bindDept/bindDept'
          })
        })
        .catch(() => {
          wx.navigateTo({
            url: '/pages/bindDept/bindDept'
          })
        });
    }

		let that=this
		let userInfo=wx.getStorageSync('userInfo')
		userInfo=JSON.parse(userInfo).userId
		 get('wx/uAddr/list',{userInfo:userInfo}).then((res)=>{
				that.setData({
					orderListItem:res.rows,
				 })
			})
    this.setData({// 把从index页面获取到的属性值赋给详情页的my，供详情页使用    
      busType:options.busType
    })
    get('wx/addr/getAddrByBus',{busType:options.busType}).then((res)=>{
      
      if(res.code==200){
        that.setData({
          fitment:res.data,
          pickAddr:res.data[0].addressName
         })
      }else{
      
        Toast({
          type: 'fail',
          message: '获取取货地址失败，请联系管理员添加',
        
        });
      }
      
    })
    
  },
	aderssy(e){
		this.setData({
			uAddr:e.currentTarget.dataset.item
		})
		this.setData({ show: false });
	},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
		get('wx/uAddr/getUAddr',{}).then((res)=>{
      if(res.code==200){
        this.setData({
          uAddr:res.data
        })
      }else{
        Dialog.confirm({
          title: '',
          message: '请前往设置默认地址',
        })
          .then(() => {
            wx.navigateTo({
              url: '/pages/address/address?cid=qwer'
            })
          })
          .catch(() => {
            wx.navigateTo({
              url: '/pages/address/address?cid=qwer'
            })
          });
        
      }
    })
  },
	popups(){
		this.setData({ show: true });
	},
	getUserInfo(event) {
    console.log(event.detail);
  },

  onClose() {
    this.setData({ show: false });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})