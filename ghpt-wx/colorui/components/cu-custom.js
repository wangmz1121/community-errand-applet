const app = getApp();

import { tabbarIcon } from "../../mock/icon";

Component({
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true,
    multipleSlots: true
  },

  /**
   * 组件的对外属性
   */
  properties: {
    bgColor: {
      type: String,
      default: 'bg-white'
    }, 
    isCustom: {
      type: [Boolean, String],
      default: false
    },
    isHome: {
      type: [Boolean, String],
      default: false
    },
    isIndex: {
      type: [Boolean, String],
      default: false
    },
    isBack: {
      type: [Boolean, String],
      default: false
    },
    // 返回键颜色
    isBackColor: {
      type: String,
      default: false
    },
    bgImage: {
      type: String,
      default: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    tabbarIcon,
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
 
  },

  // 在组件实例进入页面节点树时执行
  attached: function() {
    
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 返回上一页
    BackPage() {
      wx.navigateBack({
        delta: 1
      });
    },

    // 返回主页
    toHome(){
      wx.reLaunch({
        url: '/pages/home/home',
      })
    }
  }
})