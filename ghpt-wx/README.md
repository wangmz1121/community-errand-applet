<<<<<<< HEAD
小唐帝企业文化有限公司 创司之作
=======
# 微信小程序基础脚手架

#### 介绍
1 、小程序项目基础结构
       1）全局request接口请求封装，错误处理；2）全局api接口,ip域名地址统一管理；
       3)  全局静态资源路径统一管理； 4）功能类函数统一管理；
2、功能demo
       1）高德地图sdk插件；2）css动画效果；3）图片压缩上传； 4）画报分享；
       5）瀑布流布局；6）分享微信好友/群； 7）isbn条形码图书识别

#### 软件架构
 | api => request请求接口url地址
 | colorui => colorui 样式库
 | mock => 静态资源路径，模拟列表数据
 | pages => 业务文件
    | index => 首页
    | error => 错误页
    | compontent => 公共组件
    .....
 | static => 静态资源
    | css => 自定义公共样式
    | image => 本地图片
 | utils => 工具类函数
    | amam-wx => 高德地图sdk
    | formData => 参数formdata格式化
    | ipconfig => 接口请求域名地址,第三方依赖key
    | mimeMap => formdata依赖
    | request => 接口请求全局对应处理
    | util => 工具类共用函数，cemo表情转码解码，日期时间格式化
 | wx-canvas-2d => canvas第三方库
 
#### 安装教程

1.  priject.config.json文件中appid填入个人测试号appid即可

#### 使用说明

1.  如需使用isbn图书识别码功能需获取isbnkey详情见iconfig.js有对应注释
2.  如需前后端接口联调,iconfig.js填入后端ip地址或域名（如是ip地址请勾选合法域名校验）
3.  reqeust.js 需根据具体后端返回字段做对应字段名称调整及接口code码回调处理

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
>>>>>>> 0419
