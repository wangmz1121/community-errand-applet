# 微信小程序基础脚手架

#### Description
1 、小程序项目基础结构
       1）全局request接口请求封装，错误处理；2）全局api接口,ip域名地址统一管理；
       3)  全局静态资源路径统一管理； 4）功能类函数统一管理；
2、功能demo
       1）高德地图sdk插件；2）css动画效果；3）图片压缩上传； 4）画报分享；
       5）瀑布流布局；6）分享微信好友/群； 7）isbn条形码图书识别

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
