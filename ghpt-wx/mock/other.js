// 静态模拟数据

// 初始数据
const firstData = [
  {
    remark: "图片1",
    id: "62",
    funcPhoto: "../../static/image/waterfall/7.jpeg"
  },
  {
    remark: "图片2",
    id: "63",
    funcPhoto: "../../static/image/share/town.jpeg"
  },
  {
    remark: "图片3",
    id: "64",
    funcPhoto: "../../static/image/share/ip.jpg"
  },
  {
    remark: "图片4",
    id: "65",
    funcPhoto: "../../static/image/waterfall/7.jpeg"
  },
  {
    remark: "图片5",
    id: "66",
    funcPhoto: "../../static/image/share/ip.jpg"
  },
  {
    remark: "图片6",
    id: "62",
    funcPhoto: "../../static/image/share/town.jpeg"
  },
  {
    remark: "图片7",
    id: "63",
    funcPhoto: "../../static/image/share/ip.jpg"
  },
  {
    remark: "图片8",
    id: "64",
    funcPhoto: "../../static/image/share/ip.jpg"
  },
  {
    remark: "图片9",
    id: "65",
    funcPhoto: "../../static/image/share/town.jpeg"
  },
  {
    remark: "图片10",
    id: "66",
    funcPhoto: "../../static/image/share/ip.jpg"
  },
  {
    remark: "图片11",
    id: "65",
    funcPhoto: "../../static/image/waterfall/7.jpeg"
  }
]

// 加载数据
const pushData = [
  {
    remark: "图片11",
    id: "66",
    funcPhoto: "../../static/image/share/ip.jpg"
  },
  {
    remark: "图片12",
    id: "65",
    funcPhoto: "../../static/image/share/town.jpeg"
  },
]

const webSocket = {
  // 我的头像
  myPortrait: 'https://thirdwx.qlogo.cn/mmopen/vi_32/Lce6RUD2EdwRDsAhTzjJF8J7Yv3XCCAiasdDFNHyTCmvBfer5PTHY6ADbYjRHdThUaiab8jd8ZbS0T2GUKMv7jjg/132',
  // 其他头像
  otherPortrait: 'https://thirdwx.qlogo.cn/mmopen/vi_32/iaMhsEREDzicgibWeqkOrN1gFm13zjP7o6JUJkPicz2iaicx0icPGWx7gVO0npAGUGcn2nEvUVLRfouA7icJgMYDGfQe1A/132'
}
 
export {
  pushData,
  firstData,
  webSocket
}