// tabbar 底部栏，顶部栏icon
const tabbarIcon = {
  top: '../../static/image/index/tabbar-top.ico',
  right: '../../../static/image/index/wd.png',
  left: '../../../static/image/index/ddlb.png',
  middle: '../../../static/image/index/tabbar-middle.png'

}

// 首页icon
const indexIcon = {
  indexBg: '../../static/image/index/index-bg.gif',
  phonelogin: '../../static/image/index/phone-login.png'
}

// 画报图片路径
const pictorialIcon = {
  // 头像
  'headPortrait': '../../static/image/share/ip.jpg',
  // 二维码
  'qrCode': '../../static/image/share/qrcode.jpg',
  // isbn条形码
  // 'isbn': 'https://ss0.baidu.com/7Po3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/83025aafa40f4bfb7cc3cd56064f78f0f73618a4.jpg'
}

// 其他
const otherIcon = {
  qrCode: '../../static/image/index/qr.jpg',
  goodsImg: '../../static/image/share/good.jpg',
  blank: '../../static/image/waterfall/blank.png'
}

export {
  indexIcon,
  otherIcon,
  tabbarIcon,
  pictorialIcon
}