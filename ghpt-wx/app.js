//app.js

import { request } from 'utils/request';

import { chatApi }  from 'api/chat';

import { loginAuthorApi } from 'api/loginAuthorization';

App({
  onLaunch: function() {
    // 每次打开都重新获取token，先移除。防止token过期
    wx.removeStorageSync('token');
    const that = this;
    // 登录 测试
    wx.login({
      success: async function(loginRes) {
        // 发送 res.code 到后台换取 token
        // const res = {
        //   method: 'post',
        //   url: loginAuthorApi.login(),
        //   data: {
        //     code: loginRes.code
        //   }
        // };
        // const returnValue = await request(res);
        // 将后台返回的token存到本地缓存中以供后续每个接口使用
        // wx.setStorageSync('token', returnValue.token);
        /*
          * 获取token后通知index 首页已经获取到了token
          * 可执行首页加载数据,解决index.js 异步接口优先于app.js 
          * 异步接口执行回调, 这时候token并未获取到
        */ 
        // if(that.tokenCallback) {
        //   that.tokenCallback(returnValue.token);
        // }
        // 创建socket连接
        // wx.connectSocket({
        //   url: chatApi.connectSocket(returnValue.id),
        // })
        // 断开重连
        // wx.onSocketClose((result) => {
        //   wx.connectSocket({
        //     url: chatApi.connectSocket(returnValue.id),
        //   })
        // })
      }
    });
 
    // 获取系统信息
    wx.getSystemInfo({
      success: e => {
        this.globalData.systemInfo = e;
        this.globalData.StatusBar = e.statusBarHeight;
        let custom = wx.getMenuButtonBoundingClientRect();
        this.globalData.Custom = custom;  
        this.globalData.CustomBar = custom.bottom + custom.top - e.statusBarHeight;

        let modelmes = e.model;
        // 首页如果是广告页，需要计算全屏图片高度
        // 得到安全区域高度
        if (modelmes.search('iPhone X') != -1) { //IPhoneX底部大约为68rpx、
          this.globalData.hasSafeArea = true;
          // 登录页广告图片高度 = 手机高度 - 小程序头部拦（约等于65px） - iphonex系列安全底部（68px）
          this.globalData.loginImgHeight = e.windowHeight - 65 - 68;
        }
        else{
          this.globalData.hasSafeArea = false;
          // 登录页广告图片高度 = 手机高度 - 小程序头部拦（约等于65px）
          this.globalData.loginImgHeight = e.windowHeight - 65;
        }
      }
    })
  },

  onShow: function(options) {
    
  },

  globalData: {
    systemInfo: null, // 客户端设备信息
    userInfo : null
  },

  onLaunch () {
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        console.log('onCheckForUpdate====', res)
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          console.log('res.hasUpdate====')
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好，是否重启应用？',
              success: function (res) {
                console.log('success====', res)
                // res: {errMsg: "showModal: ok", cancel: false, confirm: true}
                if (res.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~'
            })
          })
        }
      })
    }
  }

})