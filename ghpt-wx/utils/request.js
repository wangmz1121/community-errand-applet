
// 封装request()请求携带参数
async function request(requestRes) {
  let token = '';
  // 获取本地存储的token
  token = wx.getStorageSync('token');

  const {
    data: data,
    url: url, 
    method: method,
    contentType: contentType 
  } = requestRes;

  return new Promise((reslove, reject) => {
    wx.request({
      url,
      data: data || {},
      method,
      // X-xp-Token => 后台规范携带token的名称
      header: {
        'X-xp-Token': token,
        'content-type': contentType || 'application/json'
      },
      success: (res) => {
        const {
          errMsg,
          statusCode,
          // errmsg，errno 后台返回的字段名称
          data: {
            errmsg: successMsg,
            errno: successCode,
            data: successData
          }
        } = res
        // 后台 errno = 0请求成功, 其他跳转错误页 (具体逻辑根据后端接口文档或者业务需求进行对应处理)
        switch(successCode) {
          case 0:
            reslove(successData);
            break;
          default:
            wx.navigateTo({
              url: `/pages/error/error/error?url=${url}&errmsg=${successMsg}&successCode=${successCode}`
            })
            break;
        }
      },
      fail: (e) => {
        // 电脑上：
        // 有网络连线但没信号的msg是request:fail timeout
        // 无网络连接时是request:fail
        // 手机的报错:  request:fail -错误码 错误描述
        if(e.errMsg.match("request:fail")) {
          wx.showToast({
            icon: 'none',
            title: "网络开小差了",
            duration: 3000
          })
          reject(e);
        } 
      }
    })
  })
}

// 封装高德web服务，请求接口（高德web服务接口返回数据格式比较特殊）
async function amapRequest(requestRes) {

  const {
    url: url,
    data: data
  } = requestRes;

  return new Promise((reslove, reject) => {
    wx.request({
      url,
      data: data || {},
      success: (res) => {
        const {
          errMsg,
          statusCode,
          data: {
            count: successCount,
            status: successStatus,
            geocodes: successGeocodes,
            infocode: successInfocode
          }
        } = res;
        // 高德接口status=1请求成功
        switch(successStatus) {
          case '1':
            // 数量等于0提示暂无结果
            if(successCount == '0') {
               wx.showToast({
                title: '暂无查询结果',
                icon: 'none',
                duration: 2000
              })
            }
            reslove(successGeocodes);
            break;
          default:
            // 抛出错误infocode
            reject(successInfocode);
            break;
        }
      },
      fail: (e) => {
        // 电脑上：
        // 有网络连线但没信号的msg是request:fail timeout
        // 无网络连接时是request:fail
        // 手机的报错:  request:fail -错误码 错误描述
        if(e.errMsg.match("request:fail")) {
          wx.showToast({
            icon: 'none',
            title: "网络开小差了",
            duration: 3000
          })
          reject(e);
        } 
      }
    })
  })
}

// 封装上传图片请求
async function uploadFile(uploadFileRes) {
  let token = '';
  // 获取本地存储的session
  token = wx.getStorageSync('token');

  const {
    url: url,
    name: name,
    filePath: filePath,
    formData: formData
  } = uploadFileRes

  return new Promise((reslove, reject) => {
    // 将图片以文件格式上传
    wx.uploadFile({
      url,
      name,
      filePath,
      formData,
      header: {
        // 后台定义字段名称
        token
      },
      success(res) {
        const {
          errMsg,
          statusCode,
          data: resData,
        } = res
        const data = JSON.parse(resData);
        // 后台定义code值，这里默认=0请求成功
        switch(data.code) {
          case 0:
            reslove(data.data);
            break;
          default:
            wx.navigateTo({ 
              url: `/pages/error/error/error?successCode=${data.code}&type=u2`
            })
            break;
        }
      },
      fail: (e) => {
        if(e.errMsg.match("request:fail")){
          wx.showToast({
            icon: 'none',
            title: "网络开小差了",
            duration: 3000
          })
          reject(e);
        }
      }
    })
  })
}

// isbn查询request请求
async function isbnRequest(isbnRequestRes) {
  const { url: url } = isbnRequestRes
  return new Promise((reslove,reject) => {
    wx.request({
      url,
      success: (res) => {
        // 后台ret=0请求成功,返回书本信息
        if(res.data.ret == 0) {
          reslove(res.data.data);
        }
        else{
          reject(res.data.data);
        }
      },
      fail: (err) => {
        if(err.errMsg.match("request:fail")){
          wx.showToast({
            icon: 'none',
            title: "网络开小差了",
            duration: 3000
          })
          reject(err);
        }
      },
    })
  })
  .catch((e) => {
    // 用这个把错误打印出来，仔细查，能解决的。(有时候免费的isbn查询网址会有变更，需要在微信平台添加普通接口访问白名单)
    return wx.showToast({
      title: '服务维护中，请稍候重试',
      icon: 'none'
    })
  })
}


export {
  request,
  uploadFile,
  amapRequest,
  isbnRequest
}
