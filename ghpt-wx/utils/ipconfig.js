/*
  * 域名或者ip地址后面记得 + / ,如高德地图web服务接口地址格式
  * downloadfile合法域名需要配置 https://ss0.baidu.com 域名白名单
*/ 

// 普通接口
// 线上
// const ipConFig = 'http://192.168.0.107:8080/wx';
// const ipConFig = 'https://46942r7177.goho.co/wx';
const ipConFig = 'https://ghpt.ymgh.top/wx';

// 开发
// const ipConFig = '';

// isbn查询地址 
// const isbnConFig = 'https://api.zuk.pw/situ/book/isbn/';

// isbn图书条形码apikey
/*
  * 多次不带apikey或带错误的apikey，将会禁止当天该ip的请求
  * 一个apiky一天最多接受来自3个ip的请求，一旦超过，将会禁止当天该apikey的所有请求
  * 所以请各位自行申请，apikey
  * 获取地址 https://jike.xyz/api/isbn.html 扫码文章中的二维码获取然后填入下方
*/ 
const isbnApiKey = '';


// websoket接口
// 线上
// const soketConFig = '';
// 开发
const soketConFig = '';


// // 高德地图web服务接口地址
// const amapIpConFig = 'https://restapi.amap.com/';

// // 高德地图web服务key 
// const amapWebKey = '225e7c65011f0f3abda13e40b963fdc9';

// // 高德地图小程序key 
// const amapWeChatKey = 'ecb2731f994d67c1cc7d12e45cb28f5d';

module.exports = {
  ipConFig,
  // isbnConFig,
  // isbnApiKey,
  soketConFig
  // amapIpConFig,
  // amapWebKey,
  // amapWeChatKey
}