# 社区跑腿小程序

#### 介绍
社区内取快递，取外卖，取团购，帮买帮送等功能。

#### 软件架构
##### PC管理端：
    1.维护小区信息
    2.审核配送员
    3.修改相关协议
    4.监控订单
    5.管理用户地址
    6.各小区自定义收费标准
##### 小程序：
    1.发布订单
    2.维护地址
    3.查看订单
    4.骑手接单
    5.订单实时监控
    6.新订单短信，邮件提醒配送员
    7.接单等订单进程实时短信通知用户
    8.对接微信支付
    


#### 安装教程：

  1.即开即用  


#### 使用说明

 1.小程序二维码：  
<img src="https://wang-hh.oss-cn-beijing.aliyuncs.com/guHong/ghCode.jpg" width="300" height="300" />
  
 2.B站演示视频：https://www.bilibili.com/video/BV1k34y1E7P2/  
 3.功能清单：  
 <img src="https://wang-hh.oss-cn-beijing.aliyuncs.com/guHong/functionList" width="600" height="350" />

  
#### 联系我们

1.  微信：g11331021
2.  私信我进入开发组,使用程序中的接口查看效果。


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

